SHELL:=/bin/bash

build: git app migrate

git:
	git fetch --progress --prune origin
	git submodule init
	git submodule update --recursive

app:
	./develop build
	./develop up -d
	./basic/composer.phar selfupdate
	./develop exec -u www-data php7 ./composer.phar install

migrate:
	./develop exec -u www-data php7 ./yii migrate --interactive=0

migrate/down:
	./develop exec -u www-data php7 ./yii migrate/down

migrate/create:
	./develop exec -u www-data php7 ./yii migrate/create $(NAME)

console:
	./develop exec -u www-data php7 ./yii $(COMMAND)

test:
	./develop exec -u www-data php7 vendor/bin/codecept run
