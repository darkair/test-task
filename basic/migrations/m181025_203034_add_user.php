<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m181025_203034_add_user
 */
class m181025_203034_add_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => Schema::TYPE_PK,
            'username' => Schema::TYPE_STRING,
            'accessToken' => Schema::TYPE_STRING
        ]);

        // NOTE: Т.к. нельзя завязываться на изменяемый код, создаем данные нативно
        \Yii::$app->db->createCommand()->insert(
            'users',
            [
                'username' => 'test1',
                'accessToken' => 'token1'
            ]
        )->execute();

        \Yii::$app->db->createCommand()->insert(
            'users',
            [
                'username' => 'test2',
                'accessToken' => 'token2'
            ]
        )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
