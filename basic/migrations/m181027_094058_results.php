<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m181027_094058_results
 */
class m181027_094058_results extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('results', [
            'id' => Schema::TYPE_PK,
            'hash' => Schema::TYPE_STRING,
            'userId' => Schema::TYPE_INTEGER,
            'request' => Schema::TYPE_TEXT,
            'result' => Schema::TYPE_INTEGER
        ]);
        $this->createIndex('hash', 'results', 'hash');
        $this->addForeignKey('results_fk', 'results', 'userId', 'users', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('results');
    }
}
