<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Result;
use app\models\User;

class ServiceController extends Controller
{
    /**
     * Proccess main algorithm for test task
     * @param int $num number for search
     * @param array $seq sequence of numbers
     * @param int $userId Id of user for store result
     */
    public function actionIndex($num, array $seq, $userId = null)
    {
        // Чтобы не ловить ошибки типов входных параметров, типизируем вручную
        $num = (int)$num;

        if ($userId !== null) {
            $userId = (int)$userId;
            $user = User::findOne($userId);
            if (empty($user)) {
                return ExitCode::NOUSER;
            }
        }
        $model = new Result();
        $model->setAttributes([
            'num' => $num,
            'seq' => $seq,
        ]);
        if ($userId !== null) {
            $model->userId = $userId;
        }
        if (!$model->validate()) {
            return ExitCode::DATAERR;
        }

        // Find previously result
        $prevModel = Result::findOne(['hash' => $model->getHash()]);
        if ($prevModel) {
            $model = $prevModel;
        } else {
            $model->calcResult();
            if (!$model->save()) {
                return ExitCode::UNSPECIFIED_ERROR;
            }
        }
        echo $model->result;
        return ExitCode::OK;
    }
}
