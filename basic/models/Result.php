<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;

class Result extends ActiveRecord
{
    public $num = null;
    public $seq = null;

    public static function tableName()
    {
        return 'results';
    }

    public function fields()
    {
        return [
            'userId',
            'request',
            'result'
        ];
    }

    public function rules()
    {
        return [
            [['num', 'seq'], 'required'],
            ['seq',
                function ($attribute, $params, $validator) {
                    if (!is_array($this->$attribute)) {
                        $this->addError($attribute, $attribute.' is not array');
                    }
                }
            ],
            ['seq', 'filter',
                'filter' => function ($val) {
                    array_walk($val, function(&$item, $k) {
                        $item = (int)$item;
                    });
                    return $val;
                },
                'skipOnEmpty' => true
            ],
            ['userId', 'exist', 'targetRelation' => 'user', 'message' => 'User does not exist'],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    public function getRequestJson()
    {
        return Json::encode([
            'num' => $this->num,
            'seq' => $this->seq
        ]);
    }

    public function getHash()
    {
        return md5((int)$this->userId . $this->getRequestJson());
    }

    public function afterFind()
    {
        $arr = Json::decode($this->request);
        if (isset($arr['num'])) {
            $this->num = $arr['num'];
        }
        if (isset($arr['seq'])) {
            $this->seq = $arr['seq'];
        }
        return parent::afterFind();
    }

    public function beforeSave($insert)
    {
        // Encode params to json, although could be use getRawBody(), but I want to control parameter "request"
        $this->request = $this->getRequestJson();
        $this->hash = $this->getHash();
        return parent::beforeSave($insert);
    }

    /**
     * Расчет результата
     */
    public function calcResult()
    {
        // NOTE: Т.к. возможно сделать две реализация алгоритма в зависимости начала обхода слева или справа, то реализуем оба
        //       Проще всего было бы развернуть массив для обхода справа налево, но есть ограничение по расходу памяти (

        // Начинаем слева
        $left = 0;
        $right = count($this->seq) - 1;
        $countLeft = 0;
        $countRight = 0;
        do {
            // Ищем слева первый N
            for (;  $left <= $right && $this->seq[$left] != $this->num;  ++$left) {}
            if ($left > $right) {
                break;
            }
            ++$countLeft;

            // Ищем справа первый не N
            for (;  $left < $right && $this->seq[$right] == $this->num;  --$right) {}
            if ($left >= $right) {
                break;
            }
            ++$countRight;
            ++$left;
            --$right;
        } while(true);
        $resultLeft = ($countLeft == 0 || $countRight == 0 || $countLeft != $countRight)
            ? -1
            : $right + 1;

        // Начинаем справа
        $left2 = 0;
        $right2 = count($this->seq) - 1;
        $countLeft2 = 0;
        $countRight2 = 0;
        do {
            // Ищем справа первый не N
            for (;  $left2 <= $right2 && $this->seq[$right2] == $this->num;  --$right2) {}
            if ($left2 > $right2) {
                break;
            }
            ++$countRight2;

            // Ищем слева первый N
            for (;  $left2 <= $right2 && $this->seq[$left2] != $this->num;  ++$left2) {}
            if ($left2 > $right2) {
                break;
            }
            ++$countLeft2;
            ++$left2;
            --$right2;
        } while(true);
        $resultRight = ($countLeft2 == 0 || $countRight2 == 0 || $countLeft2 != $countRight2)
            ? -1
            : $right2 + 1;

        // Возвращаем то разделение, которое удалось
        if ($resultLeft != -1) {
            $this->result = $resultLeft;
        } elseif ($resultRight != -1) {
            $this->result = $resultRight;
        } else {
            $this->result = -1;
        }
        return $this->result;
    }
}
