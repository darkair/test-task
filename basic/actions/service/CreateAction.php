<?php

namespace app\actions\service;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\web\ServerErrorHttpException;
use yii\web\BadRequestHttpException;
use app\models\Result;
use app\models\User;

class CreateAction extends \yii\rest\CreateAction
{
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $model = new $this->modelClass([
            'scenario' => $this->scenario
        ]);

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        // Set user id
        $user = User::findOne(Yii::$app->user->id);     // Use our model, it has username
        $model->userId = $user->id;
        if (!$model->validate()) {
            throw new BadRequestHttpException('['.implode('], [',$model->getErrorSummary(true)).']');
        }

        // Find previously result
        $prevModel = $this->modelClass::findOne(['hash' => $model->getHash()]);
        if ($prevModel) {
            $model = $prevModel;
        } else {
            $model->calcResult();

            if ($model->save()) {
                $response = Yii::$app->getResponse();
                $response->setStatusCode(201);
                $id = implode(',', array_values($model->getPrimaryKey(true)));
                $response->getHeaders()->set('Location', Url::toRoute([$this->viewAction, 'id' => $id], true));
            } elseif (!$model->hasErrors()) {
                throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
            }
        }
        return ["result" => $model->result];
    }
}