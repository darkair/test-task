<?php

namespace tests\models;

class ResultTest extends \Codeception\Test\Unit
{
    public function testCalcResultFalse()
    {
        $resultModel = new \app\models\Result();

        $resultModel->num = 5;

        $resultModel->seq = [];
        $res = $resultModel->calcResult();
        expect($res)->equals(-1);

        $resultModel->seq = [5];
        $res = $resultModel->calcResult();
        expect($res)->equals(-1);

        $resultModel->seq = [5, 5, 5];
        $res = $resultModel->calcResult();
        expect($res)->equals(-1);

        $resultModel->seq = [2];
        $res = $resultModel->calcResult();
        expect($res)->equals(-1);

        $resultModel->seq = [2, 3, 4];
        $res = $resultModel->calcResult();
        expect($res)->equals(-1);
    }

    public function testCalcResultTrue()
    {
        $resultModel = new \app\models\Result();

        $resultModel->num = 5;

        $resultModel->seq = [5, 5, 2, 5];
        $res = $resultModel->calcResult();
        expect($res)->equals(1);

        $resultModel->seq = [5, 5, 2, 3, 5];
        $res = $resultModel->calcResult();
        expect($res)->equals(2);

        $resultModel->seq = [5, 5, 2, 3, 4, 5];
        $res = $resultModel->calcResult();
        expect($res)->equals(3);

        $resultModel->seq = [5, 2, 3, 4, 5];
        $res = $resultModel->calcResult();
        expect($res)->equals(3);

        $resultModel->seq = [0, 5, 1, 5, 5, 2, 4, 5];
        $res = $resultModel->calcResult();
        expect($res)->equals(4);

        $resultModel->seq = [5, 5, 1, 7, 2, 3, 5];
        $res = $resultModel->calcResult();
        expect($res)->equals(4);
    }
}
