<?php

namespace tests\models;

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    public function testFindUserByAccessToken()
    {
        expect_that($user = User::findIdentityByAccessToken('token1'));
        expect($user->username)->equals('test1');

        expect_not(User::findIdentityByAccessToken('test'));
    }
}
